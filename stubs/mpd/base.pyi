import enum
from _typeshed import Incomplete
from typing import ClassVar

VERSION: tuple
HELLO_PREFIX: str
ERROR_PREFIX: str
SUCCESS: str
NEXT: str
def escape(text): ...

class FailureResponseCode(enum.Enum):
    _use_args_: ClassVar[bool] = ...
    _member_names_: ClassVar[list] = ...
    _member_map_: ClassVar[dict] = ...
    _value2member_map_: ClassVar[dict] = ...
    _unhashable_values_: ClassVar[list] = ...
    _unhashable_values_map_: ClassVar[dict] = ...
    _member_type_: ClassVar[type[object]] = ...
    _value_repr_: ClassVar[None] = ...
    NOT_LIST: ClassVar[FailureResponseCode] = ...
    ARG: ClassVar[FailureResponseCode] = ...
    PASSWORD: ClassVar[FailureResponseCode] = ...
    PERMISSION: ClassVar[FailureResponseCode] = ...
    UNKNOWN: ClassVar[FailureResponseCode] = ...
    NO_EXIST: ClassVar[FailureResponseCode] = ...
    PLAYLIST_MAX: ClassVar[FailureResponseCode] = ...
    SYSTEM: ClassVar[FailureResponseCode] = ...
    PLAYLIST_LOAD: ClassVar[FailureResponseCode] = ...
    UPDATE_ALREADY: ClassVar[FailureResponseCode] = ...
    PLAYER_SYNC: ClassVar[FailureResponseCode] = ...
    EXIST: ClassVar[FailureResponseCode] = ...
    @classmethod
    def __init__(cls, value) -> None: ...

class MPDError(Exception): ...
class ConnectionError(MPDError): ...
class ProtocolError(MPDError): ...

class CommandError(MPDError):
    def __init__(self, error) -> None: ...

class CommandListError(MPDError): ...
class PendingCommandError(MPDError): ...
class IteratingError(MPDError): ...

class mpd_commands:
    def __init__(self, *commands, **kwargs) -> None: ...
    def __call__(self, ob): ...
def mpd_command_provider(cls): ...

class Noop:
    mpd_commands: ClassVar[None] = ...

class MPDClientBase:
    NOOP: ClassVar[Noop] = ...
    def __init__(self, use_unicode = None) -> None: ...
    @classmethod
    def add_command(cls, name, callback): ...
    def noidle(self): ...
    def command_list_ok_begin(self): ...
    def command_list_end(self): ...
    @property
    def use_unicode(self): ...

class _NotConnected:
    def __getattr__(self, attr): ...

class MPDClient(MPDClientBase):
    idletimeout: ClassVar[None] = ...
    _timeout: ClassVar[None] = ...
    _wrap_iterator_parsers: ClassVar[list] = ...
    timeout: Incomplete
    def __init__(self, use_unicode = None) -> None: ...
    def connect(self, host, port, timeout = None): ...
    def disconnect(self): ...
    def fileno(self): ...
    def command_list_ok_begin(self): ...
    def command_list_end(self): ...
    @classmethod
    def add_command(cls, name, callback): ...
    @classmethod
    def remove_command(cls, name): ...
    def idle(self, *args): ...
    def close(self, *args): ...
    def kill(self, *args): ...
    def plchangesposid(self, *args): ...
    def listall(self, *args): ...
    def listallinfo(self, *args): ...
    def listfiles(self, *args): ...
    def lsinfo(self, *args): ...
    def addid(self, *args): ...
    def config(self, *args): ...
    def replay_gain_status(self, *args): ...
    def rescan(self, *args): ...
    def update(self, *args): ...
    def channels(self, *args): ...
    def commands(self, *args): ...
    def listplaylist(self, *args): ...
    def notcommands(self, *args): ...
    def tagtypes(self, *args): ...
    def urlhandlers(self, *args): ...
    def list(self, *args): ...
    def readmessages(self, *args): ...
    def listmounts(self, *args): ...
    def listneighbors(self, *args): ...
    def listpartitions(self, *args): ...
    def add(self, *args): ...
    def addtagid(self, *args): ...
    def binarylimit(self, *args): ...
    def clear(self, *args): ...
    def clearerror(self, *args): ...
    def cleartagid(self, *args): ...
    def consume(self, *args): ...
    def crossfade(self, *args): ...
    def delete(self, *args): ...
    def deleteid(self, *args): ...
    def delpartition(self, *args): ...
    def disableoutput(self, *args): ...
    def enableoutput(self, *args): ...
    def findadd(self, *args): ...
    def load(self, *args): ...
    def mixrampdb(self, *args): ...
    def mixrampdelay(self, *args): ...
    def mount(self, *args): ...
    def move(self, *args): ...
    def moveid(self, *args): ...
    def moveoutput(self, *args): ...
    def newpartition(self, *args): ...
    def next(self, *args): ...
    def outputvolume(self, *args): ...
    def partition(self, *args): ...
    def password(self, *args): ...
    def pause(self, *args): ...
    def ping(self, *args): ...
    def play(self, *args): ...
    def playid(self, *args): ...
    def playlistadd(self, *args): ...
    def playlistclear(self, *args): ...
    def playlistdelete(self, *args): ...
    def playlistmove(self, *args): ...
    def previous(self, *args): ...
    def prio(self, *args): ...
    def prioid(self, *args): ...
    def random(self, *args): ...
    def rangeid(self, *args): ...
    def rename(self, *args): ...
    def repeat(self, *args): ...
    def replay_gain_mode(self, *args): ...
    def rm(self, *args): ...
    def save(self, *args): ...
    def searchadd(self, *args): ...
    def searchaddpl(self, *args): ...
    def seek(self, *args): ...
    def seekcur(self, *args): ...
    def seekid(self, *args): ...
    def sendmessage(self, *args): ...
    def setvol(self, *args): ...
    def shuffle(self, *args): ...
    def single(self, *args): ...
    def sticker_delete(self, *args): ...
    def sticker_set(self, *args): ...
    def stop(self, *args): ...
    def subscribe(self, *args): ...
    def swap(self, *args): ...
    def swapid(self, *args): ...
    def toggleoutput(self, *args): ...
    def unmount(self, *args): ...
    def unsubscribe(self, *args): ...
    def volume(self, *args): ...
    def count(self, *args): ...
    def currentsong(self, *args): ...
    def readcomments(self, *args): ...
    def stats(self, *args): ...
    def status(self, *args): ...
    def outputs(self, *args): ...
    def playlist(self, *args): ...
    def listplaylists(self, *args): ...
    def decoders(self, *args): ...
    def find(self, *args): ...
    def listplaylistinfo(self, *args): ...
    def playlistfind(self, *args): ...
    def playlistid(self, *args): ...
    def playlistinfo(self, *args): ...
    def playlistsearch(self, *args): ...
    def plchanges(self, *args): ...
    def search(self, *args): ...
    def sticker_find(self, *args): ...
    def sticker_get(self, *args): ...
    def sticker_list(self, *args): ...
    def albumart(self, *args): ...
    def readpicture(self, *args): ...
