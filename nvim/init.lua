require("opts")
require("globals")

do
	local install_location = vim.fs.joinpath(vim.fn.stdpath("data")--[[@as string]], "rocks")
	local rocks_config = {
		auto_sync = true,
		luarocks_config = {
			variables = {
				CC = "cc",
				LD = "cc",
			},
		},
		rocks_path = vim.fs.normalize(install_location),
	}

	vim.g.rocks_nvim = rocks_config

	local luarocks_path = {
		vim.fs.joinpath(rocks_config.rocks_path, "share", "lua", "5.1", "?.lua"),
		vim.fs.joinpath(rocks_config.rocks_path, "share", "lua", "5.1", "?", "init.lua"),
	}
	package.path = package.path .. ";" .. table.concat(luarocks_path, ";")

	local luarocks_cpath = {
		vim.fs.joinpath(rocks_config.rocks_path, "lib64", "lua", "5.1", "?.so"),
		vim.fs.joinpath(rocks_config.rocks_path, "lib", "lua", "5.1", "?.so"),
	}
	package.cpath = package.cpath .. ";" .. table.concat(luarocks_cpath, ";")

	vim.opt.runtimepath:append(
		vim.fs.joinpath(rocks_config.rocks_path, "lib", "luarocks", "rocks-5.1", "rocks.nvim", "*")
	)
end

if not pcall(require, "rocks") then
	require("rocks-bootstrap")
end

pcall(require, "colorscheme")
