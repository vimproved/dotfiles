vim.opt.expandtab = false
vim.opt.shiftwidth = 8
vim.opt.softtabstop = 8
vim.opt.tabstop = 8

vim.opt.cursorline = true
vim.opt.linebreak = true
vim.opt.number = true

vim.opt.undofile = true
