-- folke's plugins seem to trigger missing-fields even though the setup works
-- fine without them present. Disable the diagnostic.
---@diagnostic disable-next-line:missing-fields
require("lazydev").setup({
	library = {
		{ path = "luvit-meta/library", words = { "vim%.uv" } },
	},
})
