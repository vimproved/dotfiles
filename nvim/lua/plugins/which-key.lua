require("which-key").add({
	{ "<M-h>", "<C-w>h", desc = "Go to the left window" },
	{ "<M-j>", "<C-w>j", desc = "Go to the down window" },
	{ "<M-k>", "<C-w>k", desc = "Go to the up window" },
	{ "<M-l>", "<C-w>l", desc = "Go to the right window" },
	{ "<leader>w", proxy = "<C-w>", group = "window" },
})
