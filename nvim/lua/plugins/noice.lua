-- folke's plugins seem to trigger missing-fields even though the setup works
-- fine without them present. Disable the diagnostic.
---@diagnostic disable-next-line:missing-fields
require("noice").setup({
	lsp = {
		override = {
			["vim.lsp.util.convert_input_to_markdown_lines"] = true,
			["vim.lsp.util.stylize_markdown"] = true,
			["cmp.entry.get_documentation"] = true,
		},
	},
})
