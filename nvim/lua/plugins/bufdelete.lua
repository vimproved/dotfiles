require("which-key").add({
	{ "<M-q>", require("bufdelete").bufdelete, desc = "Delete buffer", mode = { "n", "v" } },
})
