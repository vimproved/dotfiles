require("bufferline").setup({
	highlights = require("rose-pine.plugins.bufferline"),
	options = {
		offsets = {
			{
				filetype = "neo-tree",
				highlight = "Directory",
				text = "Files",
				text_align = "center",
			},
		},
	},
})

local wk = require("which-key")
for i = 1, 9 do
	wk.add({
		{ "<M-" .. i .. ">", "<cmd>BufferLineGoToBuffer " .. i .. "<CR>", desc = "Go to buffer " .. i },
	})
end

wk.add({
	{ "<M-0>", "<cmd>BufferLineGoToBuffer -1<CR>", desc = "Go to last buffer" },
})
