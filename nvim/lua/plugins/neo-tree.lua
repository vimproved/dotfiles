require("neo-tree").setup({
	close_if_last_window = true,
	filesystem = {
		filtered_items = {
			hide_dotfiles = false,
		},
	},
})

require("which-key").add({
	{ "<leader>t", group = "neo-tree" },
	{
		"<leader>tf",
		function()
			require("neo-tree.command").execute({
				action = "show",
				position = "left",
				source = "filesystem",
				toggle = true,
			})
		end,
		desc = "Toggle filesystem pane",
	},
	{
		"<leader>tg",
		function()
			require("neo-tree.command").execute({
				position = "float",
				source = "git_status",
			})
		end,
		desc = "Show git_status pane",
	},
})

require("neo-tree.command").execute({
	action = "show",
	position = "left",
	source = "filesystem",
})
