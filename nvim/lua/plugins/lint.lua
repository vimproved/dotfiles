require("lint").linters_by_ft = {
	python = { "mypy" },
	["yaml.ansible"] = { "ansible_lint" },
}

vim.api.nvim_create_autocmd("BufWritePost", {
	callback = function()
		require("lint").try_lint()
	end,
})

vim.api.nvim_create_autocmd("BufEnter", {
	callback = function()
		require("lint").try_lint()
	end,
})
