local conform = require("conform")

conform.setup({
	formatters_by_ft = {
		c = { "clang-format" },
		cpp = { "clang-format" },
		json = { "jq" },
		lua = { "stylua" },
		python = { "ruff_fix", "ruff_format", "ruff_organize_imports" },
		rust = { "rustfmt" },
	},
})

require("which-key").add({
	{ "<leader>f", conform.format, desc = "Format current buffer" },
})
